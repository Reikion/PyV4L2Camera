import sys
from glob import glob
from os import path
from PyV4L2Camera.pyV4L2CameraLib import ffi

import inspect

class CameraException(Exception):
    pass

def _get_library_path():
    """
    Function to get libPyV4l2.so library path needed by ffi.dlopen.
    After installation with setuptools library gets longer name than declared,
    i.e libPyV4L2Camera.cpython-35m-x86_64-linux-gnu.so
    """
    modulepath = inspect.getfile(CameraException)
    libdir = path.dirname(modulepath)
    libpath = None

    for f in glob(libdir + '/libPyV4L2*.so'):
        libpath = f

    return libpath

class Camera:
    """
    Class for to handle camera. Its only and main function is to get_frame.
    device: str - camera path, i.e. '/dev/video0'
    """

    __libpath = _get_library_path()

    def __init__(self, device: str):
        """
        Class constructor.
        device: str - camera path, i.e. '/dev/video0'
        """

        self.__lib = ffi.dlopen(Camera.__libpath)
        self.device = device.encode('utf-8')
        self.__buffer = None
        self.__camera_fd = None

    def open(self):
        """
        Function to prepare camera for getting frames.
        """
        cam_state = self.__lib.open_camera(self.device)

        if cam_state.is_success == False:
            raise CameraException(ffi.string(cam_state.errorstring))

        self.__camera_fd = cam_state.fd
        self.__buffer = self.__lib.create_frame_buffer()

    def close(self):
        """
        Function to end work with camera gracefully.
        """
        self.__lib.free_frame_buffer(self.__buffer)
        self.__lib.close_camera(self.__camera_fd)

    def get_frame(self):
        """
        Get frame from camera. It returns bytes from default stream in default format
        depending on camera model, i.e it can be JPEG, PNG, XOXO, whatever
        """

        bytes_count = self.__lib.get_frame(self.__camera_fd, self.__buffer);
        frame = ffi.buffer(self.__buffer, bytes_count)

        return frame

    def __enter__(self):
        self.open(self)
        return self

    def __exit__(self, type, value, traceback):
        self.close(self)
        return True



#include <stdio.h>
#include <stdlib.h>

#include "v4l2.h"

int main(int argc, char *argv[])
{
    CameraState camera_state;
    camera_state = open_camera("/dev/video0");

    CameraFrame frame;
    frame = get_frame(camera_state.fd);

    FILE *fp;
    fp = fopen("frame.jpg", "wb");
    fwrite(frame.frame, frame.bytes, 1, fp);
    fclose(fp);

    close_camera(camera_state.fd);

    return 0;
}

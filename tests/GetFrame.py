from PyV4L2Camera.camera import Camera

cam = Camera('/dev/video0')
cam.open()

for i in range(1, 15):
    frame = cam.get_frame()
    with open('frame_python_' + str(i) + '.jpg', 'wb') as f:
        f.write(frame)

cam.close()

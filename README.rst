PyV4L2Camera
============
Python binding to get camera frame with V4L2 API.

How to install
==============

Pip
---
.. code::

    python setup sdist
    pip install dist/PyV4L2Camera-1.*.tar.gz


Development
-----------
For easier development library provides make to build C extension library, which
is located in *lib* directory.

To install dependencies::

    python install -r requirements.txt

Compile::

    cd lib
    make

API
===
Using API is self-explaining. You can find examples in *examples* directory.

class PyV4L2Camera.camera.Camera(device: str)

   Class for to handle camera. Its only and main function is to
   get_frame. device: str - camera path, i.e. '/dev/video0'

   close()

      Function to end work with camera gracefully.

   get_frame()

      Get frame from camera. It returns bytes from default stream in
      default format depending on camera model, i.e it can be JPEG,
      PNG, XOXO, whatever

   open()

      Function to prepare camera for getting frames.

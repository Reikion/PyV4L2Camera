#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/videodev2.h>


#include "v4l2.h"

const int buffer_size = 1024 * 1024 ; // 1 MiB

CameraState open_camera(const char* device)
{
    int fd = open(device, O_RDWR);

    CameraState state = {
        .fd = fd
    };
    
    if (fd == -1) {
        state.is_success = false;
        state.errorstring = strerror(errno);
    } else {
        state.is_success = true;
        state.errorstring = NULL;
    }

    return state;

}


int get_frame(int camera_fd, char *buffer)
{

    int count = read(camera_fd, buffer, buffer_size);

    if (count == buffer_size) {
        return -1; // buffer is too small for this camera, recompilation with bigger buffer needed
    } 

    return count;
}


char* create_frame_buffer()
{
    return malloc(buffer_size);
}


void free_frame_buffer(char *buffer)
{
    free(buffer);
}

CameraState close_camera(int camera_fd)
{
    //TODO error handling
    close(camera_fd);

    CameraState state = {
        .fd = camera_fd,
        .is_success = true
    };

    return state;
}



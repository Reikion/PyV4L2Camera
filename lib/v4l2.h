#include <stdbool.h>

typedef struct CameraState {
    int fd;
    bool is_success;
    const char *errorstring;
} CameraState;

CameraState open_camera(const char* device);

int get_frame(int camera_fd, char *buffer);
char* create_frame_buffer();
void free_frame_buffer(char *buffer);

CameraState close_camera(int camera_fd);

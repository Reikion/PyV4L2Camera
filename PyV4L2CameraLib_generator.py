from cffi import FFI

ffi = FFI()

ffi.set_source("PyV4L2Camera/pyV4L2CameraLib", None)
ffi.cdef("""
    typedef struct CameraState {
        int fd;
        bool is_success;
        const char *errorstring;
    } CameraState;

    CameraState open_camera(const char* device);

    int get_frame(int camera_fd, char *buffer);
    char* create_frame_buffer();
    void free_frame_buffer(char *buffer);

    CameraState close_camera(int camera_fd);
""")


if __name__ == "__main__":
    ffi.compile()

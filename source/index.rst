.. PyV4L2Camera documentation master file, created by
   sphinx-quickstart on Thu Apr  7 22:44:56 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyV4L2Camera's documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

   README
   PyV4L2Camera


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


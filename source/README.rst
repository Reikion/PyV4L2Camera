PyV4L2Camera
============
Python binding to get camera frame with V4L2 API.

How to install
==============

Pip
---
.. code::

    python setup sdist
    pip install dist/PyV4L2Camera-1.*.tar.gz


Development
-----------
For easier development library provides make to build C extension library, which
is located in *lib* directory.

To install dependencies::

    python install -r requirements.txt

Compile::

    cd lib
    make

API
===
Using API is self-explaining. You can find examples in *examples* directory.



#!/usr/bin/env python

from setuptools import setup, Extension, find_packages

c_module = Extension('PyV4L2Camera.libPyV4L2Camera',
                    sources = ['lib/v4l2.c'])


setup(name='PyV4L2Camera',
      version='1.0.0',
      description='Simple module to get frame by frame from camera',
      author='Reik',
      setup_requires=["cffi>=1.0.0"],
      cffi_modules=["PyV4L2CameraLib_generator.py:ffi"],
      install_requires=["cffi>=1.0.0"],
      py_modules=['PyV4L2Camera.camera', 'PyV4L2Camera.pyV4L2CameraLib'],
      ext_modules=[c_module]
     )
